//traductions
var label=browser.i18n.getMessage("options").split(",");
document.getElementById("labelPlace").textContent=label[0];
document.getElementById("boutonSauve").textContent=label[1];
document.getElementById("boutonDefautPosition").textContent=label[3];
document.getElementById("boutonDefautOrbes").textContent=label[3];
document.getElementById("labelUtc").textContent=label[6];//utc
labelUtc.style.color="red";
document.getElementById("titreOrbes").textContent=label[4];
document.getElementById("autoSet").textContent=label[5];

labelsGauche=browser.i18n.getMessage("labelsGauche").split(",");
//document.getElementById("autoSet").textContent=label1[21];

label2=browser.i18n.getMessage("labelsDroite").split(",");
document.getElementById("labelLux").textContent=label2[26].split("- ")[1];

var aspects=browser.i18n.getMessage("aspects").split(",");
var posDef=["3","4","5","6","luxDef"];//lieu,utc,latitude,longitude,liminosité
var posVal=["paris",,48.51,2.21,0.15];
page_options=1;

async function saveOptions(e) {
    var clef="zodiaque";
    var valeur=[];
    var x,y,element;
    for (var i=0;i<posDef.length;i++){
        valeur[i]=document.getElementById(posDef[i]).value;
                 //ajout des décimales à latitude et longitude
                if (i==2 || i==3) {
                    element=document.getElementById(String(i+3+"b"));
                    x=Number(valeur[i]);
                    y=Number(element.value/100);
                    if (x<0) y=-y;
                    valeur[i]=String((x+y).toFixed(2));//2 décimales
                }
    }
    //orbes
    for (i=0;i<=9;i++){
        valeur[i+5]=document.getElementById("numA"+i).value;    
    }
    //enregistrement
    e.preventDefault();
    var storing=browser.storage.local.set({[clef] : valeur});
    storing.then(document.getElementById("ok").textContent="ok ");//+label[7]);
    //recharge Zodiaque à partir de l'id de l'onglet
    let id=localStorage.getItem('tabId');
    if (id){
        let removing=await browser.tabs.remove(Number(id));
        let creating = await browser.tabs.create({url: '../html/zodiaque.html'});
        localStorage.setItem('tabId', creating.id);
    }
}

//position par défaut (sauf lieu et utc)
document.getElementById("boutonDefautPosition").addEventListener("click", () => {
    var x;
    for (var i=0;i<=posDef.length;i++){
        x=posVal[i];
        if (i==2 || i==3) x=splitLatLong(i,x);
        document.getElementById(posDef[i]).value =x; //posVal[i];
    }
});

//orbes par défaut
var orbes=[15,2,2,4,8,8,2,0.5,10,0.7];
document.getElementById("boutonDefautOrbes").addEventListener("click",() => {
    for (var i=0;i<orbes.length;i++){
        document.getElementById("numA"+i).value=orbes[i];
    }
});

//longitude/latitude : séparation parie entière et décimale
function splitLatLong(i,x){
    var element=document.getElementById(String(i+3+"b"));
    var y=Math.trunc(x);//partie entière
    element.value=Math.round(Math.abs(x-y)*100);//partie décimale
    x=y;
    return x;
}

function restoreOptions() {
    
    function setCurrentChoice(result) {
        var b=[],x,y,element;
        var objTest = Object.keys(result);
        if (objTest.length){b=result.zodiaque;}
            //lieu, utc, longitude,latitude (si valeur absente, écriture de la valeur par défaut sauf pour lieu)
            for (var i=0;i<posDef.length;i++){
                if (b[i]) x=b[i];
                else if(!b[i] && i>0) x=posVal[i];
                if (i==2 || i==3) x=splitLatLong(i,x);
                document.getElementById(posDef[i]).value=x;
            }
            //orbes (si absent, écriture de la valeur par défaut sinon pas d'aspects affichés !)
            for (i=0;i<=9;i++){
                document.getElementById("labelA"+i).textContent=aspects[i];
                if (i==9) document.getElementById("labelA9").textContent=aspects[17]; //uranien
                if (b[i+5]) document.getElementById("numA"+i).value=b[i+5];
                else document.getElementById("numA"+i).value=orbes[i];
            }
    }
    
    function onError(error) {
        console.log(`Error: ${error}`);
    }
    var getting = browser.storage.local.get("zodiaque");
    getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("boutonSauve").addEventListener("click", saveOptions);
