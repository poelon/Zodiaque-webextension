async function checkTab(){
    let id=localStorage.getItem('tabId');
    if (id){
        browser.tabs.remove(Number(id));
    }
    let creating = await browser.tabs.create({url: '../html/zodiaque.html'});
    //stocke l'id de l'onglet Zodiaque
    localStorage.setItem('tabId', creating.id);
    //ferme la fenetre popup sinon curseur souris non visible avec Firefox, nécessité de cliquer
    window.close();
}

checkTab();
